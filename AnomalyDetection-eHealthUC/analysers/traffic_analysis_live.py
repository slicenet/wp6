import pyshark
from datetime import datetime
import time
import math
import numpy as np

def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn


## The model also adapts to the packet reading rate, it is to show a live demo, data coming from the testbed within eHealth slice.
## Some other improvements can be done in future work. 

#############################################################################################################################
# 																															#
# DATA Traffic from PCAP File                                                                                               #
#																															#
#############################################################################################################################
# Choose the fileName accordingly
#Windows
#fileName = "c:/Users/truont3/Documents/current/Projects/LABs/Lab/TeleHealth/spilttraffic/abnormal-traffic.pcap"

#Linux
#fileName = "../traindata/abnormal-traffic.pcap"

#cap = pyshark.FileCapture(fileName, display_filter='tcp') # , display_filter="tcp.dstport==8888 && tcp.sport==8888")	

#############################################################################################################################
#																															# 
# DATA Traffic from LIVE CAPTURE, network interface                                                                         #
#																															#
#############################################################################################################################

# Choose the ethernet inteface to capture accordingly
cap = pyshark.LiveCapture(interface='ens33', bpf_filter='tcp')
cap.sniff(packet_count=1)

#############################################################################################################################
#																															# 
# Output Files                                                                                                              #
#																															#
#############################################################################################################################
# Only used for offline training, for extracting the features with smoothing for offline learning/turning the parameters
#import csv
#Windows
#outputFileName = "c:/Users/truont3/Documents/current/Projects/LABs/Lab/TeleHealth/spilttraffic/abnormal-traffic-output.csv"
#Linux
#outputFileName = "../abnormal-traffic-output.csv"

#out = open(outputFileName, "w");

#output = "time,eppw,ppw\n"
#out.write(output)


import pandas as pd
import matplotlib.pyplot as plt

#############################################################################################################################
#																															# 
# Defined Variable that can be tuned for different performance                                                              #
#																															#
#############################################################################################################################

windowsize = 0.1 #0.01 # second
windowtime = 0
interval = 100

# Total packets counted, Total data based on payload length
totalPackets = 0
totalData = 0

#eHealth traffic, e.g., number of packets counted, is from and to port 8888, server=10.202.2.42, client=? 
ehealthtraffic = 0

#Take the reference time for sliding window
timereference=float(cap[0].sniff_timestamp)
#print (timereference)
# Building a dataframe
column_names = ['time', 'eppw', 'ppw']
df = pd.DataFrame(columns = column_names)

plotstarting = 50	# control the time to start plotting
#############################################################################################################################
#    																														# 
# 1. Online Updating the Standard Deviation Distribution                                                                    #
#																															#
#############################################################################################################################
'''
ppw_std = 8.617038412354908
ppw_mean = 16.657
ppw_min = -9.194115237064725 	
ppw_max = 42.50811523706473		

eppw_std = 9.016063387088625
eppw_mean = 16.199
eppw_min = -10.849190161265874	
eppw_max = 43.24719016126588	
'''

# Online updating the std distribution
def cal_std(df, ppw):
	# Set upper and lower limit to 3 standard deviation
	ppw_std = np.std(df['ppw'])
	ppw_mean = np.mean(df['ppw'])
	anomaly_cut_off1 = ppw_std * 2	# this value is set based on the learning process.

	ppw_min  = max(ppw_mean - anomaly_cut_off1, 0)
	ppw_max = ppw_mean + anomaly_cut_off1

	print "PPW:"
	print "\tstd: ", ppw_std
	print "\tmean: ", ppw_mean
	print "\tmin: ", ppw_min
	print "\tmax: ", ppw_max

	eppw_std = np.std(df['eppw'])
	eppw_mean = np.mean(df['eppw'])
	anomaly_cut_off2 = eppw_std * 2		

	eppw_min  = max(eppw_mean - anomaly_cut_off2, 0)
	eppw_max = eppw_mean + anomaly_cut_off2
	
	print "EPPW:"
	print "\tstd: ", eppw_std
	print "\tmean: ", eppw_mean
	print "\tmin: ", eppw_min
	print "\tmax: ", eppw_max
	
	# ppw is chosen based on the learning process, data observations
	if ppw > ppw_max or ppw < ppw_min:
		return "Abnormal"
	else:
		return "Normal"	
#############################################################################################################################
#																															# 
# 2. Boxplot                                                                                                                #
#																															#
#############################################################################################################################
''' Value calcucated from normal traffic
PPW Boxplot:
        median:  17.0
        upper_quartile:  19.0
        lower_quartile:  13.0
        IQR, Interquartile Range :  6.0
        upper_whisker:  28
        lower_whisker:  4
EPPW Boxplot:
        median:  17.0
        upper_quartile:  19.0
        lower_quartile:  13.0
        IQR, Interquartile Range :  6.0
        upper_whisker:  28
        lower_whisker:  4
'''		

# Online updating the boxplot distribution
def cal_boxplot_value(data, predictvalue):
	median = np.median(data)
	upper_quartile = np.percentile(data, 75)
	lower_quartile = np.percentile(data, 25)

	iqr = upper_quartile - lower_quartile
	upper_whisker = data[data <= upper_quartile + 2*iqr].max()
	lower_whisker = max(data[data >= lower_quartile - 2*iqr].min() , 0)

	print "Boxplot: "
	print "\tmedian: ", median
	print "\tupper_quartile: ", upper_quartile
	print "\tlower_quartile: ", lower_quartile
	print "\tIQR, Interquartile Range : ", iqr
	print "\tupper_whisker: ", upper_whisker
	print "\tlower_whisker: ", lower_whisker
	
			
	if predictvalue < lower_whisker or predictvalue > upper_whisker:				
		return "Abnormal"
	else:		
		return "Normal"	
	
#############################################################################################################################
#																															# 
# 3. DBScan Clustering   #density based spatial clustering of applications with noise                                       #
# eps: maximum distance between two samples for them to be considered as in the same cluster.                               #
#																															#
#############################################################################################################################
from sklearn.cluster import DBSCAN

def dbscan_clustering(dfdata):
	p = dfdata['ppw']
	e = dfdata['eppw']
	dist = pd.concat([e, p], axis=1)

	data = dist.to_numpy()
	dbscan =  DBSCAN(min_samples = plotstarting, eps = 270)
	clusters = dbscan.fit_predict(data)	

	#DBSCAN performance:
	#print("ARI =", adjusted_rand_score(data, clusters).round(2))	
	n_clusters = len(set(clusters)) - (1 if -1 in clusters else 0)
	n_noise = list(clusters).count(-1)	# Skikit labels the nosy points as (-1)		
	print('Estimated number of clusters: %d' % n_clusters)
	print('Estimated number of noise points: %d' % n_noise)

	if n_noise == 0:
		return "Normal"
	else:
		return "Abnormal"

#############################################################################################################################
#																															# 
# 4. Isolation Forest   #density based spatial clustering of applications with noise                                        #
# Unsupervised Learning algorithm that belongs to the ensemble decision trees family. Isolate anomalies by assigning a score#
# to each data point. Others find the normal regions and then identifies outliers                                           #
#																															#
#############################################################################################################################
from sklearn.ensemble import IsolationForest
import numpy as np
from itertools import groupby

#Isolation Forest is an outlier detection technique that identifies anomalies instead of normal observations, not suitable for this dataset
def isolation_forest(dfdata, data):
	# Building train data
	p = dfdata['ppw']
	e = dfdata['eppw']
	train = pd.concat([e, p], axis=1)
	train_data = train.to_numpy()
	
	# Buidling test data, 1 new entry
	column_names = ['time', 'eppw', 'ppw']
	testdf = pd.DataFrame(columns = column_names)
	testdf = testdf.append(data, True)	
	test = pd.concat([testdf['ppw'], testdf['eppw']], axis=1)	
	test_data = test.to_numpy()

	with warnings.catch_warnings():
		warnings.filterwarnings("ignore",category=DeprecationWarning)
	
	#contamination, specifies the percentage of observations we believe to be outliers (scikit-learns default value is 0.1).
	clf = IsolationForest(n_estimators=2, max_samples=plotstarting)	
	clf.fit(train_data)
	
	# Predicting
	y_pred_test = clf.predict(test_data)
#	print('Predicted: ', y_pred_test[0])
	
	if y_pred_test[0] == 1:
		#print ("Predicted: Normal")
		return "Normal"
	else:
        #print ("Predicted: Not Normal")
		return "Abnormal"
	
	
	#print("Accuracy:", list(y_pred_test).count(1)/y_pred_test.shape[0])
	#dfdata['scores'] = clf.decision_function(train_data)
	#dfdata['anomaly'] = clf.predict(train_data)	
	#print (df.head(500))

	# Plotting 
	#xx, yy = np.meshgrid(np.linspace(0, 50, 50), np.linspace(0, 50, 50))
	#Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
	#Z = Z.reshape(xx.shape)

	#plt.title("IsolationForest")
	#plt.contourf(xx, yy, Z, cmap=plt.cm.Blues_r)
	#plt.scatter(train_data[:, 0], train_data[:, 1], c='white',
	#				 s=20, edgecolor='k')		 
	#plt.axis('tight')
	#plt.show()	

#############################################################################################################################
#																															# 
# 5. Random Cut Forest; Amazons unsupervised algorithm for detecting anomalies                                              #
#																															#
#############################################################################################################################
#The great thing about this algorithm is that it works with very high dimensional data.
#Also work on real-time streaming data (built in AWS Kinesis Analytics) as well as offline data.
'''
import rrcf as rrcf
def random_cut_forest(dfdata, data):
	# Building train data
	p = dfdata['ppw']
	e = dfdata['eppw']
	train = pd.concat([e, p], axis=1)
	train_data = train.to_numpy()
	n = len(dfdata)

	#Anomaly score; The likelihood that a point is an outlier is measured by its collusive displacement (CoDisp).
	#if including a new point significantly changes the model complexity (i.e. bit depth) -> that point is more likely to be an outlier.

	# Set forest parameters
	num_trees = 100
	tree_size = 256
	sample_size_range = (n // tree_size, tree_size)

	# Construct forest
	forest = []
	while len(forest) < num_trees:
		# Select random subsets of points uniformly
		ixs = np.random.choice(train_data, size=sample_size_range, replace=False)
		# Add sampled trees to forest
		trees = [rrcf.RCTree(train_data[ix], index_labels=ix)
				 for ix in ixs]
		forest.extend(trees)

	# Compute average CoDisp
	avg_codisp = pd.Series(0.0, index=np.arange(n))
	index = np.zeros(n)
	for tree in forest:
		codisp = pd.Series({leaf : tree.codisp(leaf)
						   for leaf in tree.leaves})
		avg_codisp[codisp.index] += codisp
		np.add.at(index, codisp.index.values, 1)
	avg_codisp /= index
	
	print('avg_codisp=', avg_codisp)
	if 1 == 1:
		return "Normal"
	else:
		return "Abnormal"
'''
#############################################################################################################################
#																															# 
# 6. Long Short Term Memeory, CNN                                                                                           #
#																															#
#############################################################################################################################
# under testing and improvement for accuracy

#############################################################################################################################
#																															# 
# Sending an alert to kafka server                                                                                          #
#																															#
#############################################################################################################################
from time import sleep
from kafka import KafkaProducer
import re
import json

classtypes = ['DDOS on port', 'DDOS from Source', 'Unknown']

# Input the kafkaserver IP and Port accordingly
kafkaserverIP = '10.202.2.18'
kafkaserverPort = '9092'

def send_alert(packet, df):

	producer = KafkaProducer(bootstrap_servers=[kafkaserverIP + ':' + kafkaserverPort],
							 value_serializer=lambda v: json.dumps(v).encode('utf-8'))
	

	#Construct json message
	message = 'Anormaly Detected'
	time = float(packet.sniff_timestamp)
	
	sid = '1000004' # 
	classtype = classtypes[0]
	proto = 'tcp'
	sourceIP = packet.ip.src
	sourcePort = packet.tcp.srcport
	destIP = packet.ip.dst
	destPort = packet.tcp.dstport
	#print time, sid, message, classtype, proto, sourceIP, sourcePort, destIP, destPort		

	print "\n****Anomaly Detected****\n****Sending alert message to Kafka server***"	
	data = {'time' : time, 'vnf_member_index': 0001, 'description': 'eHealth TeleStroke VNF', 'resource_id': '0001','sid' : sid, 'message' : message, 'classtype' : classtype, 'proto' : proto, 'sourceIP' : sourceIP, 'sourcePort' : sourcePort, 'destIP' : destIP, 'destPort' : destPort,  'ns_id': 'de1fe09d-fb6f-49af-b844-448f4b21269b'}
	print (data)
	
	#jsondata = json.dumps(data)
	
	producer.send('idsalerts', data)
	producer.close()

		
#############################################################################################################################
#																															#
# Parse and Analyse a received packet                                                                                       #
#																															#
#############################################################################################################################
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure
import numpy as np

def parse_and_analyse(packet):
	global windowtime, totalData, totalPackets, timedifference, timereference, ehealthtraffic, df # that will be modified in this function
	
	#print packet
	totalData= totalData + int(packet.tcp.len)
	totalPackets= totalPackets + 1
	timedifference = float(packet.sniff_timestamp)- timereference
		
	if packet.tcp.dstport == '8888' or packet.tcp.srcport=='8888':
		ehealthtraffic = ehealthtraffic + 1
		#print ("ehealthtraffic: ", ehealthtraffic)


	if timedifference > windowsize:
		timereference = float(packet.sniff_timestamp)
		#print(str(windowtime))
		# calculate total number of packets per window for eHealth traffic and for all traffic
		eppw = ehealthtraffic
		ppw = totalPackets

		# Calculate the ratio of eHealth Traffic over All traffic
		ppwratio = eppw/ppw						# packet per window ratio
	
		#print("\tPPW = " + str(ppw) + "\tEPPW = " + str(eppw) + "\tR(EPPW,PPW)=" + str(ppwratio))
	
		# Adding to dataframe	
		data = []
		entry = [windowtime, eppw, ppw]
		zipped = zip(column_names, entry)
		a_dictionary = dict(zipped)
		data.append(a_dictionary)
		#print(data)		
		df = df.append(data, True)
		#df.drop(df.index[[0]])

		ehealthtraffic = 0
		totalPackets = 0
		windowtime += 1		
				
		#out.write(str(windowtime) + "," + str(eppw) + "," + str(ppw))
		#out.write("\n")
		
		if (windowtime > plotstarting):
			ax0 = plt.subplot(331)
			ax0.cla()
			ax0.axis('off')
			ax0.text(0.2, 1, 'eHEALTH UC DEMO: Anomaly Detection in the network traffic', color='b', fontsize=16)

			alert = 0
			#------------- 1. Standard Deviation --------------------------------------------#			
			print("\n-------Standard Deviation")
			std = cal_std(df, ppw)
			print("Predicted: " + std)
			if std == 'Abnormal':
				alert += 1
				
			#------------- 2. Boxplot --------------------------------------------------------#						
			cal_boxplot_value(df['eppw'], eppw)			
			print("\n2.-------Boxplot\nEPPW Boxplot")
			print("PPW Boxplot:")
			bp = cal_boxplot_value(df['ppw'], ppw)
                        if bp == 'Abnormal':
                                alert += 1
			print("Predicted: " + bp)
			
			#------------- 3. DBScan Clustering ----------------------------------------------#
			print("\n3.-------DBScan Clustering/Unsupervised Learning")
			dbs = dbscan_clustering(df)
                        if dbs == 'Abnormal':
                                alert += 1
			print("Predicted: " + dbs)
			
			#------------- 4. Isolation Forest  ----------------------------------------------#
			print("\n4.-------Isolation Forest/Unsupervised Learning Decision Tree algorithm")
			isf = isolation_forest(df, data)
                        if isf == 'Abnormal':
                                alert += 1
			print("Predicted: " + isf)
			
			#------------- 5. Random Forest  ----------------------------------------------#
			rcf = 'NA' # random_cut_forest(df, data)			
                        if rcf == 'Abnormal':
                                alert += 1

                        if alert > 3:
                                # sending alert to kafka server
                                # plotting alert
                                ax0.text(0.3, 0.8, 'WARNING!, Anomaly Detected', color='red', fontsize=12)#, style='italic', bbox={'facecolor': $
                        else:
                                ax0.text(0.3, 0.8, 'APPROVAL: No problem detected', color='green', fontsize=12)#, style='italic', bbox={'facecol$

			
			ax0.text(0.0, 0.5, 'STD:', fontsize=8)
			ax0.text(1.2, 0.5, std, fontsize=8)
			ax0.text(0.0, 0.4, 'Boxplot:', fontsize=8)
			ax0.text(1.2, 0.4, bp, fontsize=8)
			ax0.text(0.0, 0.3, 'DBScan Clustering:', fontsize=8)
			ax0.text(1.2, 0.3, dbs, fontsize=8)
			ax0.text(0.0, 0.2, 'Isolation Forest:', fontsize=8)
			ax0.text(1.2, 0.2, isf, fontsize=8)
			#ax0.text(0.0, 0.1, 'Random Cut Forest:', fontsize=8)
			#ax0.text(1.2, 0.1, rcf, fontsize=8)
			#ax0.text(2, 0.5, 'Time-Series CNN:', fontsize=8)
			#ax0.text(3, 0.5,'NA', fontsize=8)
			ax0.text(2, 0.4, 'LSTM:', fontsize=8)
			ax0.text(3, 0.4,'NA', fontsize=8)
			
			
			if alert > 3:# SENDING the alert to Kafka server
				send_alert(packet, df)
				#perform_actuation(packet, action='None')
			
			# getting 100 entries in the df does not work
			dfx = df	#.iloc[i:windowtime]
			#i += 1
			#dfx = df[df.time > i]
			#print('length=', dfx.shape[0])

			# PLOTTING						
			ax1 = plt.subplot(334)
			plt.plot(dfx['time'], dfx['eppw'], color='b')
			plt.setp(ax1.get_xticklabels(), fontsize=6)

			# share x only
			ax2 = plt.subplot(335, sharex=ax1)
			plt.plot(dfx['time'], dfx['ppw'], color='b')
			# make these tick labels invisible
			plt.setp(ax2.get_xticklabels(), visible=False)

			# share x and y
			ax3 = plt.subplot(336, sharex=ax1, sharey=ax1)
			plt.plot(dfx['time'], dfx['eppw'], color='g')
			plt.plot(dfx['time'], dfx['ppw'], color='r')
			#plt.xlim(0.01, 5.0)
			
			#Plotting a Kernel Density Estimate (KDE) # Also plotting the eppw & ppw each windowtime
			dist = df['eppw']
			ax4 = plt.subplot(337)
			ax4.cla()
			dist.plot.kde(ax=ax4, legend=False)
			dist.plot.hist(density=True, ax=ax4, color='b')
			ax4.set_ylabel('')
			#ax4.grid(axis='y')
			ax4.set_facecolor('#d8dcd6')
			ax4.axvline(eppw_mean, ls='--', color='g')
			ax4.axvline(eppw_min, ls='--', color='g')
			ax4.axvline(eppw_max, ls='--', color='g')			

			dist = df['ppw']
			ax5 = plt.subplot(338, sharex=ax4)
			ax5.cla()
			dist.plot.kde(ax=ax5, legend=False)
			dist.plot.hist(density=True, ax=ax5, color='b')
			ax5.set_ylabel('')
			#ax5.grid(axis='y')
			ax5.set_facecolor('#d8dcd6')
			ax5.axvline(ppw_mean, ls='--', color='r')
			ax5.axvline(ppw_min, ls='--', color='r')
			ax5.axvline(ppw_max, ls='--', color='r')
			
			dist = pd.concat([df['eppw'], df['ppw']], axis=1)
			ax6 = plt.subplot(339, sharex=ax4, sharey=ax4)
			ax6.cla()
			dist.plot.kde(ax=ax6, legend=False)
			dist.plot.hist(density=True, ax=ax6, legend=False)
			#ax6.set_ylabel('Probability')
			ax6.set_ylabel('')
			#ax6.grid(axis='y')
			ax6.set_facecolor('#d8dcd6')
			ax6.axvline(eppw_mean, ls='--', color='g')
			ax6.axvline(eppw_min, ls='--', color='g')
			ax6.axvline(eppw_max, ls='--', color='g')
			ax6.axvline(ppw_mean, ls='--', color='r')
			ax6.axvline(ppw_min, ls='--', color='r')
			ax6.axvline(ppw_max, ls='--', color='r')
			

			plt.show(False)
			plt.pause(0.001)
			plt.draw()

			
		#plt.show()


#############################################################################################################################
#																															# 
# Main Program                                                                                                              #
#																															#
#############################################################################################################################	

'''
# Uncomment below to have it read offline, from a pcap file
# read from pcap
for packet in cap:
	parse_and_analyse(packet)
	
	if windowtime == 500:
		break	

'''
# read continuously from live capture
for packet in cap.sniff_continuously():
	parse_and_analyse(packet)
	
	if windowtime == 500:	# comment this out to run forever
		break

plt.show()		


