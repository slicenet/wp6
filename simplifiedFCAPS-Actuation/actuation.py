from time import sleep
import json
from json import dumps
from kafka import KafkaConsumer
import re

#############################################################################################################################
#																															# 
# Pre-defined classtypes for simplicity                                                                                     #
#																															#
#############################################################################################################################
classtypes = ['DDOS on port', 'DDOS from Source', 'Unknown']
actions = [False, False, True]
urlIP = '10.202.2.42'
urlport = '8890'
authed_user = {'username': 'thuy1', 'password': 'mypassword'}
#############################################################################################################################
#																															# 
# Registering a new user                                                                                                    #
# Basic Authentication with JWT Authorisation Token for each session                                                        #
#																															#
#############################################################################################################################
import requests
def register_user(userdata): # hard coded for now
	userurl = 'http://' + urlIP + ':' + urlport + '/api/new_user'
	
	userheaders = {
		'Content-type': 'application/json',
		'Accept': 'text/plain'
	}
	
	response = requests.post(userurl, headers=userheaders, data=json.dumps(userdata))
	
	print(response, response.json())
	
	if '201' in str(response):
		print ("User registers successfully\n")
		return True
	else:
		print ("Failed to register the user\n")
		return False

def get_auth_token(userdata):
	token = []
	userurl = 'http://' + urlIP + ':' + urlport + '/api/token'
	userheaders = {
		'Authorization': 'Basic',
		'Content-type': 'application/json',
	}
	
	#jdata=json.dumps(userdata)
	jdata = json.loads(userdata)

	post_data = {'username': 'thuy1', 'password': 'mypassword'}	
	response = requests.post(userurl, headers=userheaders, data=json.dumps(userdata), auth=(authed_user['username'], authed_user['password']))
	
	print(response, response.json())	
	token = response.json()
	token = token['token']
	
	print ("token: " + token + "\n")
	return token


#############################################################################################################################
#																															# 
# Subscribe to the 'idsalerts' topic and start consumming, if any alert then execute firewall command with rest calls       #
# Get authorisation token (jwt) for the session                                                                             #
#																															#
#############################################################################################################################

def actuation():
	consumer = KafkaConsumer(
		'idsalerts',
		 bootstrap_servers=['10.202.2.18:9092'],
		 auto_offset_reset='earliest',
		 enable_auto_commit=True,
		 auto_commit_interval_ms=10,
		 group_id='console-consumer-6208',
         value_deserializer=lambda m: json.loads(m.decode('utf-8')))
		 

	consumer.subscribe(['idsalerts'])
	consumer.poll() # dummy poll
	consumer.seek_to_end() # to end of log, and beginging of new logging

	'''
	{TopicPartition(topic=u'idsalerts', partition=0): 
	  [ConsumerRecord(topic=u'idsalerts', partition=0, offset=1964, timestamp=1591379253410L, timestamp_type=0, key=None, 
	  value=u'{"vnf_member_index": 1, "description": "eHealth TeleStroke VNF", 2 "resource_id": "0001", 3 "sid": "1000004", 
	  4 "destIP": "91.103.1.116", 5 "sourcePort": "65343", "message": "Anormaly Detected", "sourceIP": "109.78.62.76", 
	  "proto": "tcp", "classtype": "DDOS on port", "time": 1591117368.823842, "destPort": "8888", "ns_id": "de1fe09d-fb6f-49af-b844-448f4b21269b"}',
	  headers=[], checksum=None, serialized_key_size=-1, serialized_value_size=404, serialized_header_size=-1)]}
	'''
	try:
		while True:
			records = consumer.poll(1) # poll will block for 1ms if no data
			for crecord in consumer:
				print (crecord)
				print ("topic = " + crecord[0])
				#print ("topic = " + (record.TopicPartition).topic)
				#record.topic() + ", partition = " + record.partition() + ", offset = " + record.offset + ", customer = " + record.key + \
				#", country = " + record.value() + "\n")
				#consumerrecord = record[0]['ConsumerRecord']
				
				message = crecord[6]
				print ("Consuming message: ")
				print(message)				
				
				alert = message['message']
				print ("alert: " + alert)
				if alert == 'Anormaly Detected':
					classtype = message['classtype']
					if classtype == classtypes[0] and actions[0] == False:
						data = "{\"sourcePort\": \"" + message['sourcePort'] + "\", \"sourceIP\": \"" + message['sourceIP'] \
						+ "\", \"destIP\": \"" + message['destIP'] + "\", \"destPort\": \"" + message['destPort'] + "\"}"
						print (data)
						deny_port(data)
						actions[0] = True
						print "Detecting classtype=" + classtype +", Performing firewall rule to block the attackers traffic"
					elif classtype == classtypes[1] and actions[0] == False:
						data = "{\"sourcePort\": \"" + message['sourcePort'] + "\", \"sourceIP\": \"" + message['sourceIP'] \
						+ "\", \"destIP\": \"" + message['destIP'] + "\", \"destPort\": \"" + message['destPort'] + "\"}"
						print (data)
						block_source_to_port(data)
						actions[0] = True
						print "Detecting classtype=" + classtype +", Performing firewall rule to block the attackers traffic"						
				print "\n\n"
				
			actions[0] = False
	finally:
		consumer.close()

#############################################################################################################################
#																															# 
# Some Firewall REST APIs                                                                                                   #
#																															#
#############################################################################################################################

def blocksource(data):
	print "Adding firewall rule to close the port"
	lurl = url + "blockSource"
	print(lurl)
	print(headers)
	response = requests.post(lurl, headers=headers, data=json.dumps(data), auth=(authed_user['username'], authed_user['password']))
	print(response, response.json())
	print("\n")

def block_source_to_port(data):	
	lurl = url + "blockSourceToDstPort"
	print(lurl)
	print(headers)
	response = requests.post(lurl, headers=headers, data=json.dumps(data), auth=(authed_user['username'], authed_user['password']))
	print(response, response.json())
	print("\n")

def deny_port(data):
	print "Adding firewall rule to close the port"
	lurl = url + "blockPort"
	print(lurl)
	print(headers)
	response = requests.post(lurl, headers=headers, data=json.dumps(data), auth=(authed_user['username'], authed_user['password']))
	print(response, response.json())
	print("\n")
	
	
#############################################################################################################################
#																															# 
# MAIN PROGRAM                                                                                                              #
#																															#
#############################################################################################################################

userdata = "{\"username\": \"thuy1\", \"password\": \"mypassword\"}"

# 1. Uncomment below to register a new user	
register_user(userdata)

# 2. Get an auth token for this session (to avoid sending credentials every transaction
token = get_auth_token(userdata)

# 3. Preparing the url and header with jwt token
url = 'http://' + urlIP + ':' + urlport + '/firewall/'
headers = {
	'Authorization': 'Basic', #Bearer ' + token, # uncomment to use access token
	'Content-type': 'application/json',
	'Accept': 'application/json',
}


# test
#data = "{\"sourcePort\": \"80\", \"sourceIP\": \"10.20.1.1\", \"destPort\": \"222\", \"destIP\": \"8.8.8.8\"}"
#print (data)
#block_source_to_port(data)
#exit()

# 3. Run actuation loop
actuation()

#############################################################################################################################