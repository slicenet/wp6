from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory
import trollius as asyncio
import uuid
import numpy
import cPickle
import logging
import sys
import json
import facial_recognition
import opencv
import cv2
import base64

import ssl
import os

#############################################################################################################################
# 																															#
# Pre-defined variables for Archive function                                                                                #
#																															#
#############################################################################################################################
import requests

#Archiving
archiving = True	# Change to False to disable the archiving function

# IP address of the encryption service
serviceIP = '10.202.30.4'
servicePort = '8890'
eHealth_token = 's.ffmcutYWVb6balzSgpw82d48'
# For simplicity, execute HTTP call to generate an en/decryption key with selected algorithm
# assuming this step is done and the key is stored in the path eHealth_key

encryptingurl = 'http://' + serviceIP + ':' + servicePort + '/v1/transit/encrypt/eHealth_key'
decryptingurl = 'http://' + serviceIP + ':' + servicePort + '/v1/transit/decrypt/eHealth_key'

# Similarly, request an access token for executing the HTTP calls
# Assuming this step is done manually and have the token below
# eHealth_token associated with a defined eHealth_policy, restricted access, only to its own keys
headers = {
	"X-Vault-Token": eHealth_token,
}

archivefolder = "archive" # can change to store in different locations/databases.

i = 0

#@staticmethod
def archive(image_to_be_archived):
	print("Archiving...")
	try:
		global i, archiving
		if i >= 10: # demonstration purpose, only saving 20 images.
			archiving = False
			return
			
		i += 1
		#print ("converting image to base64")
		# encoding the image with base64 encoder
		image = base64.b64encode(image_to_be_archived)
		
		#print(image)

		# prepareing the payload in jason format for post request
		# and call encryption service to encrypt the image	
		response = requests.post(encryptingurl, data=json.dumps({"plaintext": image}), headers=headers)
		response.raise_for_status()

		# print("\n\n")
		print (response)
		resjson = response.json()
		encryptedimage = resjson['data'] #  json format: {u'ciphertext': u'vault:v1:J+IDpq2kd.....'}
		#print (encryptedimage)

		# store the encypted image in the archive folder			
		filename = archivefolder + '/encryptedimage' + str(i)
		print (filename)
		with open(filename, 'w') as outfile:
			json.dump(encryptedimage, outfile)
			outfile.close()

	except requests.exceptions.HTTPError as e:
		print ("Http Error:",e)
	except requests.exceptions.ConnectionError as e:
		print ("Error Connecting:",e)
	except requests.exceptions.Timeout as e:
		print ("Timeout Error:",e)
	except requests.exceptions.RequestException as e:
		print ("Exception in Something Else",e)
	

#############################################################################################################################
# 																															#
# Defined MECServerProtocol                                                                                                 #
#																															#
#############################################################################################################################
class MECServerProtocol(WebSocketServerProtocol):

    web_connections = set()
    edge_connections = dict()
    obj_uuid = 0

    def __init__(self):
        WebSocketServerProtocol.__init__(self)
        self.obj_uuid = uuid.uuid4()

    def onConnect(self, request):
        print "Client connecting: {}".format(request.peer)

        if request.path == "/get":
            print "new ambulance connection"

    def onOpen(self):
        print"WebSocket connection open."

    def onMessage(self, payload, isBinary):

        try:

            client_message = json.loads(payload)

            if client_message["type"] == "init":
                self.sendMessage(json.dumps({"type": "get_image", "mode": client_message["mode"]}))

            elif client_message["type"] == "image":
                buff = numpy.asanyarray(cPickle.loads(str(client_message["payload"])))
		cv_image = cv2.imdecode(buff, cv2.IMREAD_COLOR)
                if client_message["mode"] == "diagnostic":
                    pain_image = opencv.detect_pain(cv_image)
		    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY),100]
		    retval, buffer = cv2.imencode('.jpg', pain_image, encode_param)
                    self.sendMessage(json.dumps({"type": "receive_image", "mode": "diagnostic", "payload": cPickle.dumps(buffer)}))
                    if archiving == True:
						archive(buffer)

                if client_message["mode"] == "identify":

                    patient = facial_recognition.find_patient(cv_image)

                    if patient.has_key("ERROR"):

                        if patient["ERROR"] == "FACES":
                            self.sendMessage(json.dumps({"ERROR": "Ensure only one person is in the image", "type": "get_image", "mode": "identify"}))
                        elif patient["ERROR"] == "PERSON":
                            self.sendMessage(json.dumps({"ERROR": "Patient not recognised", "type": "get_image", "mode": "identify"}))

                    else:
                        # Add message parameters
			file = open((patient["image_path"]),"r")
			pat_img = file.read() 
                        patient["image"] = base64.b64encode(pat_img)
                        patient["mode"] = "identify"
                        patient["type"] = "patient"
                        self.sendMessage(json.dumps(patient))

        except Exception as e:
            print e

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))

    def get_assessment_id(self):
        self.assessment_id = self.assessment_id + 1
        return self.assessment_id

    def __hash__(self):
        return hash((self.obj_uuid))

    def __eq__(self, other):
        return (self.obj_uuid) == (other.obj_uuid)

    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not (self == other)


if __name__ == '__main__':

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    root.addHandler(ch)
    sslcontext = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    sslcontext.load_cert_chain('keys/server.crt', 'keys/server.key')
#    sslcontext.verify_mode = ssl.CERT_REQUIRED 
#    sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)

#    file = open("/home/sean/ip.txt","r")
#    ip = file.read()
    factory = WebSocketServerFactory(u"ws://0.0.0.0:8888")
    factory.protocol = MECServerProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, "0.0.0.0", 8888, ssl=sslcontext)
    server = loop.run_until_complete(coro)

    try:
        print 'Server Started...'
        loop.run_forever()
    except KeyboardInterrupt:
        print "Interrupt received... exiting"

    finally:
        server.close()
        loop.stop()
        loop.close()
