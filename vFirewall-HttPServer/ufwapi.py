from flask import Flask, json, jsonify, request, g, url_for
#from flask_sslify import SSLify # this is to enable https for flask, to-do-list
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
import os
import time

############################################################################################################################################################################
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
############################################################################################################################################################################
app = Flask(__name__)
#sslify = SSLify(app)
app.config['SECRET_KEY'] = 'my secret key to be defined'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

# extensions
db = SQLAlchemy(app)
auth = HTTPBasicAuth()

# Like Cookie, Session data is stored on client.
# A session with each client is assigned a Session ID.
# The Session data is stored on top of cookies/client browser and the server signs them cryptographically (with secret_key)
# To provide user session between login and logout, in this implementation, we use one-time authentication and get a jwt auth-token for a session
# then use this token to access other resources (apis)
############################################################################################################################################################################
		
# Passwords should never be stored in the clear in a user database, hashed passwords will be stored instead to prevent if this user database fall in malicious hands.
# Use simple User model, with hashed password	
from passlib.apps import custom_app_context as pwd_context
import jwt	# jason web token
class User(db.Model):	# inherit from db.Model, but add in other hash and auth token functions
	# Create user database
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key = True)
	username = db.Column(db.String(16), index = True)
	password_hash = db.Column(db.String(32))
	
	# Define hash functions, encrypting and verifying
	def hash_password(self, password):
		self.password_hash = pwd_context.encrypt(password)	#sha256_crypt hashing algorithm
		
	def verify_password(self, password):
		return pwd_context.verify(password, self.password_hash)
		
	def generate_auth_token(self, expires_in=3600):
		return jwt.encode({'id': self.id, 'exp': time.time() + expires_in}, app.config['SECRET_KEY'], algorithm='HS256')

	@staticmethod
	def verify_auth_token(token):
		print ("verifying token...")
		try:
			data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
			print ("data: ", data)
		except:
			return
		return User.query.get(data['id'])		
############################################################################################################################################################################	
@auth.verify_password # Redefine password verification with hashed password and authorisation token as well
def verify_password(username_or_token, password):
	print("verifying the user: ", username_or_token)
	# authenticate by token
	user = User.verify_auth_token(username_or_token)
	if not user:
		# if failed, authenticate with username/password
		user = User.query.filter_by(username = username_or_token).first()
		if not user or not user.verify_password(password):
			return False
	g.user = user
	return True
	
############################################################################################################################################################################

from flask import make_response
@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'UNAUTHORIZED ACCESS'}), 403)

############################################################################################################################################################################
usage = [{"id": 1, "Method": "GET", "endpoint": "/usage"}, {"id": 2, "Method": "POST", "endpoint": "/firewall/blockSource"},  {"id": 3, "Method": "POST", "endpoint": "/firewall/blockPort"}, {"id": 4, "Method": "POST", "endpoint": "/firewall/blockSourceToDstPort"}, {"id": 5, "Method": "POST", "endpoint": "/firewall/blockAnyToDstPort"}]

@app.route('/usage', methods=['GET'])
def get_usage():
    return jsonify({'usage': usage})

############################################################################################################################################################################
# Get authorisation token #$ curl -u thuy:python -i -X GET http://127.0.0.1:5000/api/token
@app.route('/api/token', methods = ['POST'])
@auth.login_required
def get_auth_token():
	content = request.get_json()
	content = json.loads(content)	# comment this if using postman
	print (content)	
	token = g.user.generate_auth_token()
	return jsonify({ 'token': token.decode('ascii') })

############################################################################################################################################################################
#$ curl -i -X POST -H "Content-Type: application/json" -d '{"username":"thuy","password":"python"}' http://127.0.0.1:5000/users
# User Registration
@app.route('/api/new_user', methods = ['POST'])
def new_user():
	content = request.get_json()
	content = json.loads(content)	# comment this if using postman
	print (content)
	username = content['username']
	password = content['password']

	if username is None:
		return jsonify({ 'username is empty': ' ' }), 404, {'Location': 'http://127.0.0.1:8890/api/new_user'}
	elif password is None:
		return jsonify({ 'password is empty': ' ' }), 404, {'Location': 'http://127.0.0.1:8890/api/new_user'}
	elif User.query.filter_by(username = username).first() is not None:
		return jsonify({ 'username already exists': username }), 404, {'Location': 'http://127.0.0.1:8890/api/new_user'}

	##Inserting data into the database is a three step process:	Create the Python object; Add it to the session; Commit the session
	user = User(username = username)
	user.hash_password(password)
	db.session.add(user)
	db.session.commit()
	return jsonify({ 'username': user.username }), 201, {'Location': 'http://127.0.0.1:5000/api/new_user'}

############################################################################################################################################################################
@app.route('/api/users', methods=['GET'])
def get_users():
	response = {'user': []}
	users = User.query.all() # [<User 1>, <User 2>, <User 3>, <User 4>, <User 5>, <User 6>]
	for user in users:
		print(user)
		response['user'].append(user.username)
		
	print("response: ", response)
	return jsonify(response)

# Delete user
#db.session.delete(me)
#db.session.commit()
def delete_all_users():
	users = User.query.all()
	for user in users: # <User 1> with type as <class '__main__.User'>
		#print(user)
		#print(type(user))
		db.session.delete(user)
		db.session.commit()

############################################################################################################################################################################	
@app.route('/firewall/blockSource', methods=['POST'])
@auth.login_required
def post_setFWBlockSource():
	content = request.get_json()
	content = json.loads(content)	# comment this if using postman
	#print (content)
	if content['sourceIP'] == myIP:
		execcmd = 'sudo ufw deny from ' + content['destIP']
	else:
		execcmd = 'sudo ufw deny from ' + content['sourceIP']
	print (execcmd)
	os.system(execcmd)
	return jsonify({'apicall': usage[1]})
#    return json.dumps({"success": True}), 201

############################################################################################################################################################################
@app.route('/firewall/blockPort', methods=['POST'])
@auth.login_required
def post_blockPort():
	content = request.get_json()
	content = json.loads(content)	# comment this if using postman
	#print (content)
	if content['sourceIP'] == myIP:
		execcmd = 'sudo ufw deny ' + content['destPort']
	else:
		execcmd = 'sudo ufw deny ' + content['sourcePort']	

	print (execcmd)
	os.system(execcmd)
	return jsonify({'apicall': usage[2]})


############################################################################################################################################################################
#Dos: Action: deny proto tcp from sourceIP to destIP port destPort/any # where it detects a source attacking to dest port, block just this source to that port
myIP = '10.202.2.42'
@app.route('/firewall/blockSourceToDstPort', methods=['POST'])
@auth.login_required
def post_setFWBlockSourceToDstPort():
	content = request.get_json()
	content = json.loads(content)	# comment this if using postman
	#print (content)
	
	if content['sourceIP'] == myIP:
		execcmd = 'sudo ufw deny proto tcp from ' + content['destIP'] + ' to ' + content['sourceIP'] + ' port ' + content['sourcePort']
	else:
		execcmd = 'sudo ufw deny proto tcp from ' + content['sourceIP'] + ' to ' + content['destIP'] + ' port ' + content['destPort']
	print (execcmd)
	os.system(execcmd)
	return jsonify({'apicall': usage[3]})

############################################################################################################################################################################
#Ddos: Action: deny proto tcp from !homenet to destIP port destPort # when multiple sources attacking to a dest port
@app.route('/firewall/blockAnyToDstPort', methods=['POST'])
@auth.login_required
def post_setFWBlockAnyToDstPort():
	content = request.get_json()
	if content['sourceIP'] == myIP:
		execcmd = 'sudo ufw deny proto tcp from any to ' + content['sourceIP'] + ' port ' + content['sourcePort']
	else:
		execcmd = 'sudo ufw deny proto tcp from any to ' + content['destIP'] + ' port ' + content['destPort']
	print (execcmd)
	os.system(execcmd)
	return jsonify({'apicall': usage[4]})


if __name__ == '__main__':
	db.create_all()	# To create / USE database mentioned in URI
	delete_all_users() # Clean up all before start
	app.run(debug=True, host='0.0.0.0', port=8890)

############################################################################################################################################################################