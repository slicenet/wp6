from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory
import trollius as asyncio
import uuid
import numpy
import pickle
import logging
import sys
import json
import facial_recognition
import opencv
import base64
import cv2

import ssl
import os

class MECServerProtocol(WebSocketServerProtocol):

    web_connections = set()
    edge_connections = dict()
    obj_uuid = 0

    def __init__(self):
        WebSocketServerProtocol.__init__(self)
        self.obj_uuid = uuid.uuid4()

    def onConnect(self, request):
        print("Client connecting: {}".format(request.peer))

        if request.path == "/get":
            print("new ambulance connection")

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):

        try:

            client_message = json.loads(payload)

            if client_message["type"] == "init":
                self.sendMessage(json.dumps({"type": "get_image", "mode": client_message["mode"]}))

            elif client_message["type"] == "image":
                buff = numpy.fromstring((base64.b64decode(client_message["payload"])), numpy.uint8)
                cv_image = cv2.imdecode(buff, cv2.IMREAD_COLOR)
                if client_message["mode"] == "diagnostic":
                    pain_image = opencv.detect_pain(cv_image)
					
                    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
                    retval, buffer = cv2.imencode('.jpg', pain_image, encode_param)
                    self.sendMessage(json.dumps({"type": "receive_image", "mode": "diagnostic", "payload": base64.b64encode(buffer)}))

                if client_message["mode"] == "identify":
                    patient = facial_recognition.find_patient(cv_image)

                    if patient.has_key("ERROR"):

                        if patient["ERROR"] == "FACES":
                            self.sendMessage(json.dumps({"ERROR": "Ensure only one person is in the image", "type": "get_image", "mode": "identify"}))
                        elif patient["ERROR"] == "PERSON":
                            self.sendMessage(json.dumps({"ERROR": "Patient not recognised", "type": "get_image", "mode": "identify"}))

                    else:
                        # Add message parameters
                        file = open((patient["image_path"]),"r")
                        pat_img = file.read()
                        patient["image"] = base64.b64encode(pat_img)
                        patient["mode"] = "identify"
                        patient["type"] = "patient"
                        self.sendMessage(json.dumps(patient))

        except Exception as e:
            print(e)

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))

    def get_assessment_id(self):
        self.assessment_id = self.assessment_id + 1
        return self.assessment_id

    def __hash__(self):
        return hash((self.obj_uuid))

    def __eq__(self, other):
        return (self.obj_uuid) == (other.obj_uuid)

    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not (self == other)


if __name__ == '__main__':

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    root.addHandler(ch)

    sslcontext = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    sslcontext.load_cert_chain('keys/server.crt', 'keys/server.key')
#    sslcontext.verify_mode = ssl.CERT_REQUIRED	
#    sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
	
    factory = WebSocketServerFactory(u"ws://0.0.0.0:8899")
    factory.protocol = MECServerProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, "0.0.0.0", 8888, ssl=sslcontext)
    server = loop.run_until_complete(coro)

    try:
        print('Server Started...')
        loop.run_forever()
    except KeyboardInterrupt:
        print("Interrupt received... exiting")

    finally:
        server.close()
        loop.stop()
        loop.close()
