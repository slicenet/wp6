#############################################################################################################
##	HISTORY OF CHANGES                                                                                     ##
## mutiples (not tracking) changes made by Dell EMC previously                                             ## 
## 04/07/2019: 	puting two connections in parallel, connecing to edge & core for Limerick Demo             ## 
##                                                                                                         ##
## 27/09/2019: 	Thuy, cleanup, add comments, fix for frames, resize frames and windows size & position     ##
##				rename variables for consistency, remove multiple definitions of the same var (winNames)   ##
##				changing public EDGE/CORE servers with public IPs + port 8888 for CORE and 8899 for EDGE   ##
##                                                                                                         ##
## 27/09/2019: 	Thuy, adding SSL/TLS certificaate for encrypted channel                                    ##
##                                                                                                         ##
##                                                                                                         ##
#############################################################################################################
###### run with python2.7, install required packets with pip ####

from autobahn.asyncio.websocket import WebSocketClientProtocol, WebSocketClientFactory
import trollius as asyncio
from twisted.logger import globalLogBeginner, textFileLogObserver
import numpy as np
import cv2
import sys
import logging
import thread
import json
import time
import base64
import cPickle

import os
import ssl

########### Adjustment for the Images, frame size and windows size & positions ############################
from win32api import GetSystemMetrics
 
Width = GetSystemMetrics(0) - 50
Height = GetSystemMetrics(1) - 50 # Some windows show the taskbar
dimension = (Width/2 - 20, Height/2 - 20)

# consistency for name of the windows
edgeInput = 'EDGE - TeleHealth Input'
edgeResult = 'EDGE - TeleHealth Assessment'

coreInput = 'CORE - TeleHealth Input'
coreResult = 'CORE - TeleHealth Assessment'
### there is option to scale the image as well (resize by scaling to fix the w/h ratio), if needed. 

########### END - Adjustment for the Images, frame size and windows size & positions ########################



########### Counters to Display no. of frames processed/per second ############################

edge_frame_counter = 1
core_frame_counter = 1
edge_stroke_counter = 1
core_stroke_counter = 1
text_edge = ('Detecting Face: Processing Frame:'+ str(edge_stroke_counter))
text_core = ('Detecting Face: Processing Frame:' + str(core_stroke_counter))
cap = cv2.VideoCapture(0)

########### END - Counters to Display no. of frames processed/per second ############################


#####################################################################################################
##																								   ##
## Below are two parallel websocket connections:                                                   ##
## first connecting to the EDGE Server; second connecting to the CORE server                       ##
## Each will display the Input and the Assessment/The result                                       ##
##                                                                                                 ##
#####################################################################################################


#WebSocket Goodness, may be worth replacing for something more efficent (in pakcet size not CPU cycles) if we continue
class MyClientProtocol(WebSocketClientProtocol):

    def __init__(self):
        WebSocketClientProtocol.__init__(self)
        self.capture_thread = 1
        self.isComplete = False
    def onConnect(self, response):
        print("Server connected: {0}".format(response.peer))

    def onOpen(self):
        print("WebSocket connection open.")
        #self.cap = cv2.VideoCapture(0)
        
        def run():
            try:
                while True:
                        global cap
                        self.encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
                        #Get (RGB) frame from camera stream
                        ret,frame, = cap.read()

                        #Add overlay text to frame before displaying it
                        cv2.putText(frame, text_edge, (1, 450), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 2, cv2.LINE_AA)
                        cv2.putText(frame, 'EDGE', (1, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 2, cv2.LINE_AA)
                        cv2.namedWindow(edgeInput, cv2.WINDOW_AUTOSIZE)
                        frame = cv2.resize(frame, dimension, interpolation = cv2.INTER_AREA)
                        cv2.imshow(edgeInput, (frame))
                        cv2.moveWindow(edgeInput, 10, 10)
                        cv2.waitKey(1)
            except Exception as e:
                print(e)
                cv2.destroyAllWindows()       
        self.capture_thread = thread.start_new_thread(run,())
        self.sendMessage(json.dumps({"type": "init", "mode": "identify"}))

    def transmit(self, mode):
        try:
            ret, frame = cap.read()
            if frame is not None:
                encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
                
                retval, buffer = cv2.imencode('.jpg', frame, encode_param) 
                try:
                    self.sendMessage(json.dumps({"type": "image", "payload": base64.b64encode(buffer), "mode": mode}))
                except Exception as e:
                    print(e)

            else:
                print('ERROR: encoding image')
                self.transmit(mode)

        except Exception as e:
            print(e)

    def onMessage(self, payload, isBinary):
               
        try:
            data = json.loads(payload)
            if data["mode"] == "identify":

                if data.has_key("ERROR"):#ERROR in this context means the face wasn't recognised, send the next frame.
                    global text_edge
                    global edge_frame_counter
                    edge_frame_counter+=1
                    text_edge = ("Detecting Face: Processing Frame:" + str(edge_frame_counter))               
                    ##print data["ERROR"]
                if data["type"] == "patient":#This means the face was recognised and patient info is sent back
                    text_edge = ("Patient Detected!") 
                    ##print data
                    self.sendMessage(json.dumps({"type": "init", "mode": "diagnostic"}))

                elif data["type"] == "get_image":
                    self.transmit(data["mode"])

            elif data["mode"] == "diagnostic":

                if data["type"] == "get_image":
                    self.transmit(data["mode"])

                if data["type"] == "receive_image":
                    buff = np.fromstring((base64.b64decode(data["payload"])), np.uint8)
                    #Counting no. of processed stroke frames
                    global edge_stroke_counter
                    edge_stroke_counter += 1
                    strtext = ("Frames Processed: " + str(edge_stroke_counter))
                    #Decode
                    cv_image = cv2.imdecode(buff, cv2.IMREAD_COLOR)
                    destRGB = cv_image#cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
                    cv2.putText(destRGB, strtext, (100, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
                    cv2.namedWindow(edgeResult, cv2.WINDOW_AUTOSIZE)
                    destRGB = cv2.resize(destRGB, dimension, interpolation = cv2.INTER_AREA)
                    cv2.imshow(edgeResult, destRGB)
                    cv2.moveWindow(edgeResult, Width/2+10, 10)
                    cv2.waitKey(1)
                    self.transmit(data["mode"])
        except:
            pass
        
    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))
        exit(0)

class MyClientProtocol2(WebSocketClientProtocol):

    def __init__(self):
        WebSocketClientProtocol.__init__(self)
        self.capture_thread = 2
        self.isComplete = False
    def onConnect(self, response):
        print("Server connected: {0}".format(response.peer))

    def onOpen(self):
        print("WebSocket connection open.")
        #self.cap = cv2.VideoCapture(0)
        
        def run():
            try:
                while True:
                        global cap
                        self.encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 100]
                        #Get (RGB) frame from camera stream
                        ret5, frame5 = cap.read()                      
                        #decompose RGB data into numpy
                        color_image2 = frame5#cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

                        #Add overlay text to frame before displaying it
                        cv2.putText(color_image2, text_core, (1, 450), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 2, cv2.LINE_AA)
                        cv2.putText(color_image2, 'CORE', (1, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 2, cv2.LINE_AA)
                        color_image2 = cv2.resize(color_image2, dimension, interpolation = cv2.INTER_AREA)
                        cv2.namedWindow(coreInput, cv2.WINDOW_AUTOSIZE)
                        cv2.imshow(coreInput, (color_image2))
                        cv2.moveWindow(coreInput, 10, Height/2+10)
                        cv2.waitKey(1)
            except Exception as e:
                print(e)
                cv2.destroyAllWindows()       
        self.capture_thread = thread.start_new_thread(run,())
        self.sendMessage(json.dumps({"type": "init", "mode": "identify"}))

    def transmit(self, mode):
        try:
            ret5, frame5 = cap.read()
            if frame5 is not None:
                encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 100]
                
                retval, buffer = cv2.imencode('.jpg', frame5, encode_param) 
                try:
                    self.sendMessage(json.dumps({"type": "image", "payload": cPickle.dumps(buffer), "mode": mode}))
                except Exception as e:
                    print(e)

            else:
                print('ERROR: encoding image')
                self.transmit(mode)

        except Exception as e:
            print(e)

    def onMessage(self, payload, isBinary):
               
        try:
            data = json.loads(payload)
            if data["mode"] == "identify":

                if data.has_key("ERROR"):#ERROR in this context means the face wasn't recognised, send the next frame.
                    global text_core
                    global core_frame_counter
                    core_frame_counter+=1
                    text_core = ("Detecting Face: Processing Frame: " + str(core_frame_counter))               
                    ##print data["ERROR"]
                if data["type"] == "patient":#This means the face was recognised and patient info is sent back
                    text_core = ("Patient Detected!") # + (str(data["name"]))
                    ##print data
                    self.sendMessage(json.dumps({"type": "init", "mode": "diagnostic"}))

                elif data["type"] == "get_image":
                    self.transmit(data["mode"])

            elif data["mode"] == "diagnostic":

                if data["type"] == "get_image":
                    self.transmit(data["mode"])

                if data["type"] == "receive_image":
                    #Decode b64 and dump into numpy array
                    buff = np.asanyarray(cPickle.loads(str(data["payload"])))
                    #Counting no. of processed stroke frames
                    global core_stroke_counter
                    core_stroke_counter += 1
                    strtext = ("Frames Processed: " + str(core_stroke_counter))
                    #Decode
                    cv_image = cv2.imdecode(buff, cv2.IMREAD_COLOR)
                    destRGB = cv_image#cv2.cvtColor(cv_image, cv2.COLOR_BGR2RGB)
                    cv2.putText(destRGB, strtext, (100, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
                    cv2.namedWindow(coreResult, cv2.WINDOW_AUTOSIZE)
                    destRGB = cv2.resize(destRGB, dimension, interpolation = cv2.INTER_AREA)					
                    cv2.imshow(coreResult, destRGB)
                    cv2.moveWindow(coreResult, Width/2+10, Height/2+10)
                    cv2.waitKey(1)
                    self.transmit(data["mode"])
        except:
            pass
        
    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))
        exit(0)


if __name__ == '__main__':

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    root.addHandler(ch)

###################################################################################################
# some test with ssl/tls	                                                                     ##
# if not certified server.ca -> throw error                                                      ##
#    self._sslobj.do_handshake()                                                                 ##
#	ssl.SSLError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed (_ssl.c:727)        ##
#                                                                                                ##
# Copy the server.crt to the capath, then replace the capath below.                              ##
#    if (not os.environ.get('PYTHONHTTPSVERIFY', '') and                                         ##
#    getattr(ssl, '_create_unverified_context', None)):                                          ##
#		ssl._create_default_https_context = ssl._create_unverified_context                       ##
###################################################################################################

    local_capath = 'c:/Users/truont3/Documents/current/Projects/LABs/Lab/TeleHealth/keys/'
    local_cafile = os.path.join(local_capath, 'server.crt')
    assert os.path.isfile(local_cafile)
	
    sslcontext = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=local_cafile, capath=local_capath)
    sslcontext.verify_mode = ssl.CERT_REQUIRED	
    sslcontext = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
	
###################################################################################################	

#    factory = WebSocketClientFactory(u'ws://91.103.1.116:8899/get')
#    factory.protocol = MyClientProtocol
#    factory.setProtocolOptions(autoPingInterval=0, autoPingTimeout=10, openHandshakeTimeout=30)
    loop = asyncio.get_event_loop()
#    coro = loop.create_connection(factory, '91.103.1.116', 8899, ssl=sslcontext)
#    loop.run_until_complete(coro)	

###### uncomment below to run the connection to the core in parallel ###############################
    factory2 = WebSocketClientFactory(u"ws://91.103.1.116:8888/get")
    factory2.protocol = MyClientProtocol2
    factory2.setProtocolOptions(autoPingInterval=0, autoPingTimeout=10, openHandshakeTimeout=30)
    coro2 = loop.create_connection(factory2, '91.103.1.116', 8888, ssl=sslcontext)
    loop.run_until_complete(coro2)
####################################################################################################

    loop.run_forever()
